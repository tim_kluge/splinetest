package main

import (
	"errors"
	"fmt"
	"github.com/gonum/matrix/mat64"
	"github.com/gonum/plot"
	"github.com/gonum/plot/plotter"
	"github.com/gonum/plot/vg"
	"image/color"
	"log"
)

var n = 10    // Data points (+ 1)
var p = 3     // Degree
var h = 5     // Control points (+ 1)
var a = 0     // Start
var b = 1     // End
var m = h + p // Knot vector size

// K ist Knotenvektor
var K = make([]float64, m+1)

func panicerr(err error) {
	if err != nil {
		panic(err)
	}
}

func approximation(points *mat64.Dense) *mat64.Dense {
	P := mat64.NewDense(h, 3, nil)

	P.SetRow(0, points.RawRowView(0))
	P.SetRow(h-1, points.RawRowView(n-1))

	A := mat64.NewDense(n-2, h-2, nil)

	for r := 0; r < n-2; r++ {
		t := (float64(r) + 1.0) / (float64(n) - 1)

		for c := 0; c < h-2; c++ {
			A.Set(r, c, Base(c+1, p-1, t))
		}
	}

	ATA := mat64.NewDense(h-2, h-2, nil)
	ATA.Mul(A.T(), A)
	ATAS := mat64.NewSymDense(h-2, ATA.RawMatrix().Data)

	var chol mat64.Cholesky

	if !chol.Factorize(ATAS) {
		panicerr(errors.New(" failed"))
	}

	L := mat64.NewTriDense(h-2, false, nil)
	L.LFromCholesky(&chol)

	U := mat64.NewTriDense(h-2, true, nil)
	U.UFromCholesky(&chol)

	for r := 0; r < h-2; r++ {
		fmt.Print("U: ")
		for c := 0; c < h-2; c++ {
			fmt.Print(L.At(r, c), " ")
		}
		fmt.Println()
	}

	/* Lt := mat64.NewDense(h-2, h-2, nil)
	Lt.Inverse(L)
	LtT := Lt.T()

	LltInv := mat64.NewDense(h-2, h-2, nil)
	LltInv.Mul(LtT, L) */

	for c := 0; c < 3; c++ {
		pointsMiddle := mat64.NewVector(n-2, nil)
		for r := 1; r < n-1; r++ {
			pointsMiddle.SetVec(r-1, points.At(r, c))
		}

		atransmiddle := mat64.NewVector(h-2, nil)
		AT := A.T()

		for r := 0; r < h-2; r++ {
			s := 0.0
			for c2 := 0; c2 < n-2; c2++ {
				s += AT.At(r, c2) * pointsMiddle.At(c2, 0)
			}
			atransmiddle.SetVec(r, s)
		}

		// atransmiddle.MulVec(A.T(), pointsMiddle)

		// ATA * controlPointsMiddle = atransmiddle
		// A * x = b

		// Untere Dreiecksmatrix * y = atransmiddle
		y := mat64.NewVector(h-2, nil)
		y.SetVec(0, atransmiddle.At(0, 0)/L.At(0, 0))
		for i := 1; i < h-2; i++ {
			e := atransmiddle.At(i, 0)
			for j := 0; j < i; j++ {
				e -= L.At(i, j) * y.At(j, 0)
			}
			r := e / L.At(i, i)
			y.SetVec(i, r)
		}

		/* y2 := mat64.NewVector(h-2, nil)
		y2.SolveVec(L, atransmiddle)
		fmt.Println("Y2: ", y2.RawVector().Data)

		fmt.Println("Y: ", y.RawVector().Data) */

		// Obere Dreiecksmatrix * x = y
		x := mat64.NewVector(h-2, nil)
		x.SetVec(h-3, y.At(h-3, 0)/U.At(h-3, h-3))
		for i := h - 4; i >= 0; i-- {
			e := y.At(i, 0)
			for j := h - 3; j > i; j-- {
				e -= U.At(i, j) * x.At(j, 0)
			}
			r := e / U.At(i, i)
			x.SetVec(i, r)
		}

		for r := 0; r < h-2; r++ {
			fmt.Print("X ", x.At(r, 0), " ")
			fmt.Println()
		}

		fmt.Println("-----")

		/* x2 := mat64.NewVector(h-2, nil)
		x2.SolveVec(U, y2)
		fmt.Println("X: ", x.RawVector().Data)
		fmt.Println("X2: ", x2.RawVector().Data) */

		for i := 1; i < h-1; i++ {
			// P.Set(i, c, controlPointsMiddle.At(i-1, 0))
			P.Set(i, c, x.At(i-1, 0))
		}
	}

	return P
}

func main() {
	// fun := func(x float64) float64 { return math.Sin(x) * 3 }

	points := mat64.NewDense(n, 3, nil)
	points.SetRow(0, []float64{-0.00154219, 0.487692, -0.480619})
	points.SetRow(1, []float64{-0.0401800, 0.499898, -0.466184})
	points.SetRow(2, []float64{-0.07866, 0.518051, -0.449909})
	points.SetRow(3, []float64{-0.11683, 0.518351, -0.431859})
	points.SetRow(4, []float64{-0.15439, 0.524526, -0.412105})
	points.SetRow(5, []float64{-0.191638, 0.530648, -0.390724})
	points.SetRow(6, []float64{-0.22798, 0.530640, -0.367801})
	points.SetRow(7, []float64{-0.263423, 0.530572, -0.343426})
	points.SetRow(8, []float64{-0.297826, 0.5280402, -0.3176967})
	points.SetRow(9, []float64{-0.331053, 0.524146, -0.290713})

	/* for i := 0; i < n; i++ {
		points.Set(i, 0, float64(a)+float64(i)*(float64(b-a)/float64(n)))
		points.Set(i, 1, fun(points.At(i, 0)))
	} */

	for i := 0; i <= p; i++ {
		K[i] = 0
	}
	for i := 1; i <= h-1-p; i++ {
		K[i+p] = float64(i) / (float64(h) - float64(p))
	}
	for i := m - p; i <= m; i++ {
		K[i] = 1
	}

	fmt.Println(K)

	// P := mat64.NewDense(h, 2, nil)
	P := approximation(points)

	/* sin := plotter.NewFunction(fun)
	sin.Color = color.RGBA{B: 255, A: 255}
	sin.Width = vg.Points(2)
	sin.Samples = 1000 */

	c := func(x float64) float64 {
		if x >= float64(a) && x <= float64(b) {
			s := 0.0
			for i := 0; i < h; i++ {
				s += P.At(i, 1) * Base(i, p-1, (x-float64(a))/float64(b-a))
			}
			return s

		}
		return 0
	}

	b12 := func(x float64) float64 {
		return Base(5, 3, x)
	}

	b12p := plotter.NewFunction(b12)
	b12p.Color = color.RGBA{G: 255, A: 255}
	b12p.Samples = 1000

	cp := plotter.NewFunction(c)
	cp.Color = color.RGBA{R: 255, A: 255}
	cp.Samples = 1000

	for i := 0; i < h; i++ {
		println(P.At(i, 0), ", ", P.At(i, 1), ",", P.At(i, 2))
	}

	pts := make(plotter.XYs, h)
	for i := 0; i < h; i++ {
		pts[i].X = P.At(i, 0)
		pts[i].Y = P.At(i, 1)
	}
	splinedata, _, _ := plotter.NewLinePoints(pts)

	plot, err := plot.New()
	if err != nil {
		log.Panic(err)
	}

	plot.Title.Text = "f"
	plot.X.Label.Text = "X"
	plot.Y.Label.Text = "Y"

	plot.Add(splinedata, cp)
	// plot.Legend.Add("sin(x)", sin)
	plot.Legend.Add("Kontrollpunkte", splinedata)
	plot.Legend.Add("Splinekurve", cp)
	plot.Legend.ThumbnailWidth = 0.5 * vg.Inch

	plot.X.Min = -1
	plot.X.Max = 1
	plot.Y.Min = -1
	plot.Y.Max = 1

	err = plot.Save(800, 800, "output.png")
	if err != nil {
		log.Panic(err)
	}
}
