#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <glm/glm.hpp>
#include <vector>

#define CURVEPOINTSIZE  40
#define POINTSSIZE    20
#define DEGREE    3

using Eigen::MatrixXd;
using namespace glm;

double calculateBasisFunction(int controllPoint, int degree, double t, MatrixXd knots) {
	//std::cout << controllPoint << " " << degree << " " << t << " " << std::endl;
	if (degree == 1) {
		if (knots(controllPoint, 0) <= t && t < knots(controllPoint + 1, 0)) {
			return 1.0;
		}
		else {
			return 0.0;
		}
	}

	double faktorANumerator = t - knots(controllPoint, 0);
	double faktorADenominator = (knots(controllPoint + degree - 1, 0) - knots(controllPoint, 0));
	double faktorA = 0.0;
	if (faktorADenominator > 0) {
		faktorA = faktorANumerator / faktorADenominator;
	}

	double faktorBNumerator = knots(controllPoint + degree, 0) - t;
	double faktorBDenominator = knots(controllPoint + degree, 0) - knots(controllPoint + 1, 0);
	double faktorB = 0.0;
	if (faktorBDenominator > 0) {
		faktorB = faktorBNumerator / faktorBDenominator;
	}

	return faktorA * calculateBasisFunction(controllPoint, degree - 1, t, knots) +
		faktorB * calculateBasisFunction(controllPoint + 1, degree - 1, t, knots);
}

MatrixXd splineApproximation(int controllPointsCount, int degree, MatrixXd knots, MatrixXd points) {
	MatrixXd controllPoints(controllPointsCount, 3);
	MatrixXd A(points.rows() - 2, controllPointsCount - 2);

	// Q(0) = P(0) und Q(last) = P(last);
	controllPoints(0, 0) = points(0, 0);
	controllPoints(0, 1) = points(0, 1);
	controllPoints(0, 2) = points(0, 2);

	int lastPoint = points.rows() - 1;
	controllPoints(controllPointsCount - 1, 0) = points(lastPoint, 0);
	controllPoints(controllPointsCount - 1, 1) = points(lastPoint, 1);
	controllPoints(controllPointsCount - 1, 2) = points(lastPoint, 2);

	//int pointsCountWithoutFirstAndLast = points.rows() - 2;
	//int controllPointsCountWithoutFirstAndLast = controllPointsCount - 2;
	std::cout << "Knots: " << std::endl;
	std::cout << knots << std::endl;

	for (int x = 0; x < points.rows() - 2; ++x) { // it �ber zeilen (Knoten)
		double t = (x + 1.0) / (points.rows() - 1);
		for (int y = 0; y < controllPointsCount - 2; ++y) { //it �ber spalten (Kontrollpunkte)
			A(x, y) = calculateBasisFunction(y + 1, degree, t, knots);
		}
	}

	std::cout << "A: " << std::endl;
	std::cout << A << std::endl;
	MatrixXd ATA = A.transpose() * A;

	Eigen::MatrixXd G((ATA).llt().matrixL());
	MatrixXd GInverseT = G.inverse().transpose();
	//MatrixXd ATAInverse = GInverseT * G;
	MatrixXd ATAInverse = ATA.inverse();
	MatrixXd pointsMiddlePart = points.block(1, 0, lastPoint - 1, 3);
	MatrixXd controllPointsMiddlePart = ATAInverse * (A.transpose() * pointsMiddlePart);

	int lastControllPoint = controllPointsCount - 1;
	for (int i = 1; i < lastControllPoint; ++i) {
		controllPoints(i, 0) = controllPointsMiddlePart(i - 1, 0);
		controllPoints(i, 1) = controllPointsMiddlePart(i - 1, 1);
		controllPoints(i, 2) = controllPointsMiddlePart(i - 1, 2);
	}

	//std::cout << ATA.inverse() << std::endl;
	//std::cout << ATAInverse << std::endl;

	return controllPoints;
}

void splineRenderer(int countPoints, int controllPointsCount, int degree, MatrixXd points) {
	if (degree <= 0 || controllPointsCount <= 1) {
		std::cout << "Bitte gib richtige Werte an" << std::endl;
		return;
	}

	int i, h;
	int m = controllPointsCount + degree;
	MatrixXd knots(m+1, 1);

	for (i = 0; i <= degree; ++i) {
		knots(i, 0) = 0;
	}

	for (i = 1; i <= controllPointsCount - 1 - degree; ++i) {
		knots(i + degree, 0) = i / static_cast<double>(controllPointsCount - degree);
	}

	for (i = m - degree; i <= m; ++i) {
		knots(i, 0) = 1;
	}

	MatrixXd controllPoints = splineApproximation(controllPointsCount, degree, knots, points);
	MatrixXd curvePoint(1, 3);

	std::vector<vec4> CP(controllPointsCount);

	std::cout << "Kontrollpunkte: " << std::endl;
	for (i = 0; i < controllPoints.rows(); ++i) {
		CP[i] = vec4(controllPoints(i, 0), controllPoints(i, 1), controllPoints(i, 2), 0);
		std::cout << CP[i].x << " " << CP[i].y << " " << CP[i].z << std::endl;
	}

	std::cout << "Kurvenpunkte:" << std::endl;

	for (i = 0; i < CURVEPOINTSIZE; ++i) {
		double t = (double)i / (CURVEPOINTSIZE - 1);
		vec4 p(0);
		for (int j = 0; j < CP.size(); ++j)
		{
			p = p + (float)calculateBasisFunction(j, degree, t, knots) * CP[j];
		}
		std::cout << p.x << " " << p.y << " " << p.z << std::endl;
	}
}

int main() {
	MatrixXd p(POINTSSIZE, 3);

	p(0, 0) = 0;
	p(0, 1) = 0;
	p(0, 2) = 0;

	p(1, 0) = 0.5;
	p(1, 1) = 3;
	p(1, 2) = 0;

	p(2, 0) = 1;
	p(2, 1) = 3.5;
	p(2, 2) = 0;

	p(3, 0) = 1.5;
	p(3, 1) = 3.7;
	p(3, 2) = 0;

	p(4, 0) = 2;
	p(4, 1) = 3.9;
	p(4, 2) = 0;

	p(5, 0) = 2.5;
	p(5, 1) = 3.7;
	p(5, 2) = 0;

	p(6, 0) = 3;
	p(6, 1) = 3.5;
	p(6, 2) = 0;

	p(7, 0) = 3.5;
	p(7, 1) = 3;
	p(7, 2) = 0;

	p(8, 0) = 4;
	p(8, 1) = 2;
	p(8, 2) = 0;

	p(9, 0) = 4.5;
	p(9, 1) = 0.6;
	p(9, 2) = 0;

	p(10, 0) = 5;
	p(10, 1) = 0.1;
	p(10, 2) = 0;

	p(11, 0) = 5.5;
	p(11, 1) = 0;
	p(11, 2) = 0;

	p(12, 0) = 6;
	p(12, 1) = 0.1;
	p(12, 2) = 0;

	p(13, 0) = 6.5;
	p(13, 1) = 0.6;
	p(13, 2) = 0;

	p(14, 0) = 7;
	p(14, 1) = 2;
	p(14, 2) = 0;

	p(15, 0) = 7.5;
	p(15, 1) = 3;
	p(15, 2) = 0;

	p(16, 0) = 8;
	p(16, 1) = 3.7;
	p(16, 2) = 0;

	p(17, 0) = 8.5;
	p(17, 1) = 4;
	p(17, 2) = 0;

	p(18, 0) = 9;
	p(18, 1) = 4.6;
	p(18, 2) = 0;

	p(19, 0) = 9.5;
	p(19, 1) = 5;
	p(19, 2) = 0;

	splineRenderer(POINTSSIZE, 10, DEGREE, p);

	return 0;
}
