package main

import (
	"fmt"
)

// KTest definiert Abbruchbedingung der rekursiv definierten B-Spline-Basisfunktion
/* func KTest(i int, u float64) float64 {
	if K[i] <= u && u < K[i+1] {
		return 1.0
	}

	return 0.0
}

func KZeroDiv(n float64, d float64) float64 {
	if d > 0 {
		return n / d
	}

	return 0.0
}*/

// BasePrint ist die B-Spline-Basisfunktion
func BasePrint(i int, p int) string {
	if p == 1 {
		return "KTest(i, u)"
	}

	fas := "(KZeroDiv(u-K[" + fmt.Sprint(i) + "], K[" + fmt.Sprint(i+p-1) + "]-K[" + fmt.Sprint(i) + "])*(" + BasePrint(i, p-1) + "))"
	fbs := "(KZeroDiv(K[" + fmt.Sprint(i+p) + "]-u, K[" + fmt.Sprint(i+p) + "]-K[" + fmt.Sprint(i+1) + "])*(" + BasePrint(i+1, p-1) + "))"

	return "(" + fas + "+" + fbs + ")"
	// return fas + "+" + fbs
}

func main() {
	r := "func Base(i int, p2 int, u float64) float64 {\r\n\t"
	for i := 1; i < 10; i++ {
		for p := 1; p < 7; p++ {
			r += "if i == " + fmt.Sprint(i) + " && p2 == " + fmt.Sprint(p) + " {\r\n"
			r += "\t\treturn " + BasePrint(i, p) + "\r\n"
			r += "\t} else "
		}
	}
	r += "{\r\n\t\tpanic(\"unknown\")\r\n\t}\r\n}"
	fmt.Println(r)
}
